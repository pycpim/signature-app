from django.urls import path

from . import views

app_name = 'signature'
urlpatterns = [
    path('', views.SignatureIndex.as_view(), name='index')
]
