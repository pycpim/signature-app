from django import forms
from django.core import validators
from signature.models import Signature

class SignatureForm(forms.ModelForm):
    class Meta:
        model = Signature
        fields = [
            "name",
            "phone",
            "constituency",
        ] 
        widgets = {
            "name": forms.TextInput(attrs={"class": "input"}),
            "phone": forms.TextInput(attrs={"class": "input", "type": "tel"}),
        }
    readt_the_petition = forms.BooleanField(
        label = "",
        required = True,
        disabled = False, 
        widget = forms.widgets.CheckboxInput(attrs={'class': 'checkbox-inline'}),
        help_text = "நான் மனுவைப் படித்தேன்/I have read the petition",
        error_messages ={'required':'Please check the box'}
    )