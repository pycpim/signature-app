from django.contrib import admin

from .models import Constituency, Signature, PhysicalSignature, ManipulateSignature

admin.site.register(Constituency)
admin.site.register(Signature)
admin.site.register(PhysicalSignature)
admin.site.register(ManipulateSignature)