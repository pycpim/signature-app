from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class Constituency(models.Model):
    PONDICHERRY_DISTRICTS = (
        ("Puducherry", "Puducherry"),
        ("Karaikal", "Karaikal"),
        ("Mahe", "Mahe"),
        ("Yanam", "Yanam"),
        ("Other State", "Other State"),
    )
    district = models.CharField(
        max_length = 20,
        choices = PONDICHERRY_DISTRICTS,
        default = "Puducherry",
    )
    name = models.CharField(
        max_length = 32,
    )
    name_ta = models.CharField(
        max_length = 64,
    )

    def __str__(self):
        return "{} | {}".format(self.name_ta, self.name)

    class Meta:
        ordering = ['-id']

class Signature(models.Model):
    name = models.CharField(
        "பெயர்/Name",
        max_length = 64,
        blank = False
    )
    constituency = models.ForeignKey(
        Constituency,
        verbose_name = "தொகுதி/Constituency",
        on_delete = models.RESTRICT
    )
    phone = PhoneNumberField(
        "தொலைபேசி/Phone",
        region = "IN",
        max_length = 13,
        blank = False,
        unique = True
    )
    readt_the_petition = models.BooleanField(
        "நான் மனுவைப் படித்தேன்/I have read the petition",
        default = False
    )
    time_stamp = models.DateTimeField(
        auto_now_add=True,
    )

    def __str__(self):
        return "{} | {} | {} | {}".format(self.name, self.phone, self.constituency, self.time_stamp)

class PhysicalSignature(models.Model):
    count = models.IntegerField()
    time_stamp = models.DateTimeField(
        auto_now_add=True,
    )
    def __str__(self):
        return "{} | {}".format(self.count, self.time_stamp)
        
class ManipulateSignature(models.Model):
    count = models.IntegerField()
    time_stamp = models.DateTimeField(
        auto_now_add=True,
    )
    def __str__(self):
        return "{} | {}".format(self.count, self.time_stamp)