# Generated by Django 4.2.3 on 2023-11-24 21:47

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('signature', '0005_signature_readt_the_petition'),
    ]

    operations = [
        migrations.AddField(
            model_name='signature',
            name='time_stamp',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='signature',
            name='constituency',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='signature.constituency', verbose_name='தொகுதி/Constituency'),
        ),
        migrations.AlterField(
            model_name='signature',
            name='name',
            field=models.CharField(max_length=64, verbose_name='பெயர்/Name'),
        ),
        migrations.AlterField(
            model_name='signature',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=13, region='IN', verbose_name='தொலைபேசி/Phone'),
        ),
        migrations.AlterField(
            model_name='signature',
            name='readt_the_petition',
            field=models.BooleanField(default=False, verbose_name='நான் மனுவைப் படித்தேன்/I have read the petition'),
        ),
    ]
