# Generated by Django 4.2.3 on 2023-11-24 19:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Constituency',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('district', models.CharField(choices=[('Puducherry', 'Puducherry'), ('Karaikal', 'Karaikal'), ('Mahe', 'Mahe'), ('Yanam', 'Yanam'), ('Other State', 'Other State')], default='Puducherry', max_length=20)),
                ('name', models.CharField(max_length=20)),
            ],
        ),
    ]
