from django.shortcuts import render
from django.views import View
from django.http import HttpResponse
from signature.models import Signature, PhysicalSignature, ManipulateSignature
from signature.forms import SignatureForm

class SignatureIndex(View):
    form_class = SignatureForm
    def get(self, request):
        data = {
            "no_of_signatures": Signature.objects.all().count() + ManipulateSignature.objects.all().last().count,
            "no_of_physical_signatures": PhysicalSignature.objects.all().last().count,
            "form": self.form_class
        }
        return render(request, 'index.html', data)

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
        else:
            data = {
                "no_of_signatures": Signature.objects.all().count() + ManipulateSignature.objects.all().last().count,
                "no_of_physical_signatures": PhysicalSignature.objects.all().last().count,
                "form": form,
            }
            return render(request, 'index.html', data)
        data = {
            "no_of_signatures": Signature.objects.all().count() + ManipulateSignature.objects.all().last().count,
            "no_of_physical_signatures": PhysicalSignature.objects.all().last().count,
        }
        return render(request, 'signature_success.html', data)

    def http_method_not_allowed(self, request, *args, **kwargs):
        return HttpResponse("method is not allowed", status=405)
