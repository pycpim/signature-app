# Signature App
This is developed from the need of launching a digital signature campaign for saving the Electricity Department of Puducherry.


## Technology stack
The technology stack used includes:
- [`Python`](https://www.python.org) ver. 3.11
- [`Django`](https://www.djangoproject.com) ver. 4.2
- [`PostgreSQL`](https://www.postgresql.org) ver. 15
- [`Gunicorn`](https://gunicorn.org) ver. 20.1
- [`Traefik`](https://traefik.io/traefik/) ver. 2.9
- [`Docker`](https://docs.docker.com/get-docker/) and [`Docker Compose`](https://docs.docker.com/compose/)

## How to use

### For development on your computer

1. Clone the repository to your computer and go to the `django-docker-template` directory:
```console
git clone https://gitlab.com/pycpim/signature-app.git
cd signature-app
```

2. Start all services locally (Postgres, Gunicorn, Traefik) using docker-compose:
```console
MY_DOMAIN=localhost docker compose -f docker-compose.debug.yml up
```

Enjoy watching the lines run in the terminal 🖥️   
And after a few seconds, open your browser at http://localhost:8090/admin/. The superuser with the login and password `admin/admin` is already created, welcome to the Django admin panel.

Django is still in Debug mode! You can work in your IDE, write code, and immediately see changes inside the container. However, you are currently using Traefik and Postgres.

Want to delete everything? No problem, the command below will stop all containers, remove them and their images.
```console
docker compose down --remove-orphans --rmi local
```

To delete the Postgre database as well, add the `-v` flag to the command:
```console
docker compose down --remove-orphans --rmi local -v
```

#### Django settings

Some Django settings from the [`settings.py`](https://gitlab.com/pycpim/signature-app/blob/main/website/website/settings.py) file are stored in environment variables. You can easily change these settings in the [`.env`](https://gitlab.com/pycpim/signature-app/blob/main/.env) file. This file does not contain all the necessary settings, but many of them. Add additional settings to environment variables if needed. 

> It is important to note the following: **never store sensitive settings such as DJANGO_SECRET_KEY or DJANGO_EMAIL_HOST_PASSWORD in your repository!** Docker allows you to override environment variable values from additional files, the command line, or the current session. Store passwords and other sensitive information separately from the code and only connect this information at system startup.

### For deployment on a server

#### Steps on a server

1. Clone the repository on your host and go to the `django-docker-template` directory:
```console
git clone https://gitlab.com/pycpim/signature-app.git
cd signature-app
```

2. Configure as described in the [Django settings](#django-settings) section or leave everything as is.

3. Run, specifying your domain:
```console
MY_DOMAIN=your.domain.com docker compose -f docker-compose.yml -f docker-compose.tls.yml up -d
```

It will take a few seconds to start the database, migrate, collect static files, and obtain a Let's Encrypt certificate. So wait a little and open https://your.domain.com in your browser. Your server is ready to work 🏆 

4. After running the containers, you can execute [manage.py commands](https://docs.djangoproject.com/en/4.2/ref/django-admin/#available-commands) using this format:
```console
docker compose exec django python manage.py check --deploy

docker compose exec django python manage.py shell
```

## Credits
This project is built based on [amerkurev/django-docker-template](https://github.com/amerkurev/django-docker-template)

## License
[MIT](LICENSE)

